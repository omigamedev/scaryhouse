#include "LevelMenuScene.h"
#include "AppDelegate.h"

bool Level::init()
{
    if(!CCNode::init()) return false;
    
    m_bg = CCScale9Sprite::create("button.png", CCRectMake(5, 5, 59, 59));
    m_bg->setAnchorPoint(CCPointZero);
    addChild(m_bg);
    
    m_label = CCLabelTTF::create("1", "Arial", 30);
    m_label->setColor(ccBLACK);
    m_label->setAnchorPoint(CCPointZero);
    addChild(m_label);
    
    setContentSize(m_bg->boundingBox().size);
    setAnchorPoint(ccp(0.5, 0.5));
    
    setEnabled(true);
    
    return true;
}

void Level::setText(const char *s)
{
    m_label->setString(s);
}

bool LevelMenuScene::init()
{
    if(!CCLayer::init()) return false;
    
    // init
    m_screen = CCDirector::sharedDirector()->getWinSize();
    m_menu = CCMenu::create();
    m_menu->setPosition(m_screen * 0.5);
    addChild(m_menu);
    m_levels = CCArray::create();
    m_levels->retain();
    int nx = 4;
    int ny = 3;
    for(int y = 0; y < ny; y++)
    {
        for (int x = 0; x < nx; x++)
        {
            Level* l = (Level*)Level::create();
            float shiftx = l->boundingBox().size.width * (nx-1) * 0.5f;
            float shifty = l->boundingBox().size.height * (ny-1) * 0.5f;
            float lx = l->boundingBox().size.width * x;
            float ly = l->boundingBox().size.height * y;
            l->setPosition(ccp(-shiftx + lx, shifty - ly));
            l->setText(CCString::createWithFormat("%d", y * nx + x)->getCString());
            l->setTarget(this, menu_selector(LevelMenuScene::onLevelClick));
            m_menu->addChild(l);
            m_levels->addObject(l);
        }
    }
    CCMenuItemLabel *back = CCMenuItemLabel::create(CCLabelTTF::create("Exit", "Arial", 30), this, menu_selector(LevelMenuScene::onMenuBack));
    back->setPosition(ccp(0, (back->boundingBox().size.height - m_screen.height) * 0.45f));
    m_menu->addChild(back);

    return true;
}

void LevelMenuScene::onLevelClick(CCObject *target)
{
    int idx = m_levels->indexOfObject(target);
    CCLOG("%d", idx);
}

void LevelMenuScene::onMenuBack(cocos2d::CCObject *target)
{
    AppDelegate::back();
}
