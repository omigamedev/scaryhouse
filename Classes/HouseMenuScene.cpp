#include "HouseMenuScene.h"
#include "LevelMenuScene.h"
#include "AppDelegate.h"

bool HouseMenuScene::init()
{
    if(!CCLayer::init()) return false;
    
    //init
    CCSize screen = CCDirector::sharedDirector()->getWinSize();
    CCMenuItem *next = CCMenuItemLabel::create(CCLabelTTF::create("Next", "Arial", 30), this, menu_selector(HouseMenuScene::onMenuNext));
    next->setPosition(ccp((-next->boundingBox().size.width + screen.width) * 0.45f, 0));
    CCMenuItem *prev = CCMenuItemLabel::create(CCLabelTTF::create("Prev", "Arial", 30), this, menu_selector(HouseMenuScene::onMenuPrev));
    prev->setPosition(ccp((prev->boundingBox().size.width - screen.width) * 0.45f, 0));
    CCMenuItem *back = CCMenuItemLabel::create(CCLabelTTF::create("Back", "Arial", 30), this, menu_selector(HouseMenuScene::onMenuBack));
    back->setPosition(ccp(0, (back->boundingBox().size.height - screen.height) * 0.45f));
    m_menu = CCMenu::create(next, prev, back, NULL);
    addChild(m_menu);
    
    m_current = NULL;
    m_houses = CCArray::create();
    m_houses->retain();
    for (int i = 0; i < 10; i++)
    {
        CCMenuItemImage *house = CCMenuItemImage::create("house.png", "house.png", this, menu_selector(HouseMenuScene::onMenuSelect));
        m_houses->addObject(house);
    }
    
    m_menuIndex = 0;
    if(!m_current) navigate(m_menuIndex, 1, false);
    
    return true;
}

void HouseMenuScene::onEnterTransitionDidFinish()
{
    
}

void HouseMenuScene::navigate(int idx, float dir, bool anim)
{
    CCSize screen = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *next = (CCMenuItemImage*) m_houses->objectAtIndex(idx);
    if(next)
    {
        m_menu->addChild(next);
        if(anim)
        {
            next->setPosition(ccp(dir * (screen.width*0.5 + next->boundingBox().size.width), 0));
            CCMoveTo *move = CCMoveTo::create(0.2, CCPointZero);
            CCSequence *seq = CCSequence::create(move, NULL);
            next->runAction(seq);
        }
        else
        {
            next->setPosition(CCPointZero);
        }
    }
    if(m_current)
    {
        CCPoint dest = ccp(-(m_current->boundingBox().size.width + screen.width*0.5) * dir, 0);
        if(anim)
        {
            CCMoveTo *move = CCMoveTo::create(0.2f, dest);
            CCCallFunc *call = CCCallFuncN::create(this, callfuncN_selector(HouseMenuScene::navigateCallback));
            CCSequence *seq = CCSequence::create(move, call, NULL);
            m_current->runAction(seq);
        }
        else
        {
            m_current->setPosition(dest);
        }
    }
    m_current = next;
}

void HouseMenuScene::navigateCallback(CCNode *target)
{
    m_menu->removeChild(target, false);
}

void HouseMenuScene::onMenuNext(cocos2d::CCObject *target)
{
    m_menuIndex = (m_menuIndex + 1) % m_houses->count();
    navigate(m_menuIndex, 1);
}

void HouseMenuScene::onMenuPrev(cocos2d::CCObject *target)
{
    m_menuIndex--;
    if(m_menuIndex < 0)
        m_menuIndex = m_houses->count() - 1;
    navigate(m_menuIndex, -1);
}

void HouseMenuScene::onMenuSelect(cocos2d::CCObject *target)
{
    AppDelegate::navigate(LevelMenuScene::scene());
}

void HouseMenuScene::onMenuBack(cocos2d::CCObject *target)
{
    AppDelegate::back();
}
