#include "RootMenuScene.h"
#include "HouseMenuScene.h"
#include "AppDelegate.h"

bool RootMenuScene::init()
{
    if(!CCLayer::init()) return false;
    
    // init
    CCMenuItemLabel *menuStart = CCMenuItemLabel::create(
        CCLabelTTF::create("Start", "Arial", 30), this, menu_selector(RootMenuScene::onMenuStart));
    CCMenuItemLabel *menuExit = CCMenuItemLabel::create(
        CCLabelTTF::create("Exit", "Arial", 30), this, menu_selector(RootMenuScene::onMenuExit));
    CCMenu *menu = CCMenu::create(menuStart, menuExit, NULL);
    menu->alignItemsVertically();
    addChild(menu);
    
    return true;
}

void RootMenuScene::onMenuStart(CCObject *target)
{
    AppDelegate::navigate(HouseMenuScene::scene());
}

void RootMenuScene::onMenuExit(CCObject *target)
{
    CCDirector::sharedDirector()->end();
}
