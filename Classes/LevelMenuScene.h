#ifndef __LEVELMENUSCENE__
#define __LEVELMENUSCENE__

#include "cocos2d.h"
#include "cocos-ext.h"
#include <vector>

using namespace cocos2d;
using namespace extension;
using namespace std;

class Level : public CCMenuItem
{
    CCScale9Sprite *m_bg;
    CCLabelTTF *m_label;
public:
    CREATE_FUNC(Level);
    virtual bool init();
    void setText(const char* s);
};

class LevelMenuScene : public CCLayer
{
    CCMenu *m_menu;
    CCArray *m_levels;
    CCSize m_screen;
    
    void onLevelClick(CCObject *target);
    void onMenuBack(CCObject *target);
    
public:
    CREATE_FUNC(LevelMenuScene);
    static CCScene* scene()
    {
        CCScene *scene = CCScene::create();
        scene->addChild(create());
        return scene;
    }
    virtual bool init();
};

#endif
