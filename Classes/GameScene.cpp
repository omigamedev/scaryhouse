#include "GameScene.h"

bool GameScene::init()
{
    if(!CCLayerColor::initWithColor(ccc4(255, 0, 0, 255)))
        return false;
    
    // init vars
    m_dragging = false;
    shadowScale = 0.25;
    m_touchL = m_touchR = NULL;
    
    // init objects
    m_screen = CCDirector::sharedDirector()->getWinSize();
    
    m_bg = CCSprite::create("level.jpg");
    m_bg->setPosition(m_screen * 0.5);
    addChild(m_bg);
    
    m_light = CCSprite::create("light-dir.png");
    m_light->setAnchorPoint(ccp(0.5, 0.25));
    m_light->setScale(4 * shadowScale);
    m_light->setPosition(ccp(340,380)*shadowScale);
    m_light->retain();
    
    m_light2 = CCSprite::create("light.png");
    m_light2->setScale(12 * shadowScale);
    m_light2->setPosition(m_light->getPosition());
    m_light2->retain();
    
    m_pgShadow = CCSprite::create("pg-shadow.png");
    addChild(m_pgShadow);
    
    m_pg = CCPhysicsSprite::create("walking-man_0000.png");
    //m_pg->setScale(0.7);
    addChild(m_pg);
    
    m_pgAnim = CCAnimation::create();
    m_pgAnim->retain();
    for(int i = 0; i < 6; i++)
    {
        m_pgAnim->addSpriteFrameWithFileName(CCString::createWithFormat("walking-man_%04d.png", i)->getCString());
    }
    m_pgAnim->setDelayPerUnit(0.2f);
    m_pgAnim->setRestoreOriginalFrame(true);
    //m_pgAnim->setLoops(-1);
    //m_pg->runAction(CCAnimate::create(m_pgAnim));
    
    m_shadow = CCRenderTexture::create(m_bg->boundingBox().size.width * shadowScale, m_bg->boundingBox().size.height * shadowScale);
    m_shadow->getSprite()->getTexture()->setAntiAliasTexParameters();
    m_shadow->getSprite()->setPosition(m_screen * 0.5);
    m_shadow->getSprite()->setScaleX(1.0/shadowScale);
    m_shadow->getSprite()->setScaleY(-1.0/shadowScale);
    m_shadow->getSprite()->setBlendFunc((ccBlendFunc){GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA});
    m_shadow->clear(0, 0, 0, 1);
    addChild(m_shadow);
    
    // init chipmuk
    m_space = cpSpaceNew();
    m_space->gravity = cpv(0, 0);
    addChild(CCPhysicsDebugNode::create(m_space));
    
    addWall(ccp(88, 424), ccp(88, 35));
    addWall(ccp(90, 516), ccp(88, 564));
    addWall(ccp(87, 251), ccp(266, 250));
    addWall(ccp(356, 251), ccp(621, 250));
    addWall(ccp(439, 248), ccp(439, 338));
    addWall(ccp(438, 428), ccp(439, 564));
    addWall(ccp(616, 161), ccp(616, 564));
    addWall(ccp(444, 72), ccp(444, 35));
    addWall(ccp(614, 73), ccp(614, 35));
    //addWall(ccp(88, 596), ccp(801, 597));
    addWall(ccp(259, 594), ccp(266, 512));
    
    addWall(CCPointZero, ccp(0, m_bg->boundingBox().size.height));
    addWall(CCPointZero, ccp(m_bg->boundingBox().size.width, 0));
    addWall(ccp(0, m_bg->boundingBox().size.height), m_bg->boundingBox().size);
    addWall(ccp(m_bg->boundingBox().size.width, 0), m_bg->boundingBox().size);
    
    addObstacle(ccp(95, 82), ccp(165, 240));
    addObstacle(ccp(98, 336), ccp(268, 433));
    addObstacle(ccp(98, 265), ccp(167, 329));
    addObstacle(ccp(543, 358), ccp(583, 401));
    addObstacle(ccp(585, 349), ccp(610, 419));
    addObstacle(ccp(445, 253), ccp(609, 335));
    addObstacle(ccp(555, 435), ccp(612, 507));
    addObstacle(ccp(99, 525), ccp(153, 587));
    addObstacle(ccp(154, 544), ccp(218, 587));
    addObstacle(ccp(363, 0), ccp(431, 45));
    addObstacle(ccp(494, 0), ccp(556, 39));
    addObstacle(ccp(561, 0), ccp(606, 56));
    addObstacle(ccp(169, 118), ccp(223, 183));
    addObstacle(ccp(622, 177), ccp(701, 504));
    
    m_pg->setCPBody(cpBodyNew(1, 1));
    cpSpaceAddBody(m_space, m_pg->getCPBody());
    cpShape *circ = cpCircleShapeNew(m_pg->getCPBody(), 30, cpvzero);
    cpSpaceAddShape(m_space, circ);
    m_pg->setPosition(m_screen * 0.5);
    
    renderShadows();
    
    setTouchEnabled(true);
    schedule(schedule_selector(GameScene::update));
    
    return true;
}

void GameScene::addWall(const CCPoint &p1, const CCPoint &p2)
{
    Wall w(p1, p2);
    m_walls.push_back(w);
    cpShape *seg = cpSegmentShapeNew(m_space->staticBody, cpv(p1.x,p1.y), cpv(p2.x,p2.y), 0);
    cpSpaceAddStaticShape(m_space, seg);
}

void GameScene::addObstacle(const CCPoint &p1, const CCPoint &p2)
{
    Obstacle o(p1, p2);
    m_objs.push_back(o);
    cpVect v[] = {cpv(p1.x,p1.y), cpv(p1.x,p2.y), cpv(p2.x,p2.y), cpv(p2.x,p1.y)};
    cpShape *seg = cpPolyShapeNew(m_space->staticBody, 4, v, cpvzero);
    cpSpaceAddStaticShape(m_space, seg);
}

void GameScene::update(float dt)
{
    m_pg->getCPBody()->v = cpvzero;
    if(m_touchL || m_touchR)
    {
        CCPoint dir2 = (m_touchRPos - m_touchRStart);
        m_touchRStart = m_touchRPos - dir2.normalize() * min(dir2.getLength(), 90.0f);
        dir2 = (m_touchRPos - m_touchRStart);
        
        CCPoint dir = (m_touchLPos - m_touchLStart);
        m_touchLStart = m_touchLPos - dir.normalize() * min(dir.getLength(), 90.0f);
        dir = (m_touchLPos - m_touchLStart);
        
        if(m_touchR)
        {
            m_light->setRotation(-CC_RADIANS_TO_DEGREES(ccpToAngle(dir2)) + 90);
            m_pg->setRotation(-CC_RADIANS_TO_DEGREES(ccpToAngle(dir2)));
        }
        else
        {
            m_light->setRotation(-CC_RADIANS_TO_DEGREES(ccpToAngle(dir)) + 90);
            m_pg->setRotation(-CC_RADIANS_TO_DEGREES(ccpToAngle(dir)));
        }
        
        //m_pg->setPosition(m_light->getPosition() / shadowScale);
        if(m_touchL) m_pg->getCPBody()->v = cpv(dir.x, dir.y) * dt * 50;
        
        // update physics
        const int iterations = 10;
        for (int i = 0; i < iterations; i++)
        {
            cpSpaceStep(m_space, 0.005f);
        }
        
        cpVect p = m_pg->getCPBody()->p * shadowScale;
        m_light2->setPosition(ccp(p.x, p.y));
        m_light->setPosition(ccp(p.x, p.y));
        m_pgShadow->setPosition(m_pg->getPosition());
        
        // render to texture
        renderShadows();
        
        // center scene
        CCSize sz(150, 100);
        CCPoint center = -getPosition() + m_screen * 0.5;
        CCPoint diff = m_pg->getPosition() - center;
        float dx = 0, dy = 0;
        if(diff.x > sz.width) dx = -diff.x + sz.width;
        if(diff.x < -sz.width) dx = -diff.x - sz.width;
        if(diff.y > sz.height) dy = -diff.y + sz.height;
        if(diff.y < -sz.height) dy = -diff.y - sz.height;
        setPosition(getPosition() + ccp(dx, dy));
    }
}

void GameScene::renderShadows()
{
    m_shadow->clear(0.3, 0.3, 0.3, 1);
    m_shadow->begin();
    
    m_light->visit();
    m_light2->visit();
    
    for(int i = 0; i < m_walls.size(); i++)
    {
        shade(m_walls[i].a, m_walls[i].b);
    }
    // ccDrawSolidRect(m_dragPos*shadowScale, m_dragPos*shadowScale+ccp(10,10), ccc4f(1, 0, 0, 1));
    
    //ccDrawLine(m_dragStart*shadowScale, m_dragPos*shadowScale);
    //for(int i = 0; i < lines.size(); i++)
    //{
    //    ccDrawLine(lines[i].s*shadowScale, lines[i].e*shadowScale);
    //}
    //ccDrawSolidRect(ccp(0,0), m_bg->boundingBox().size, ccc4f(0.01, 0.01, 0.01, 0.01));
    m_shadow->end();
}

void GameScene::draw()
{
    CCLayerColor::draw();
}

void GameScene::visit()
{
    CCLayerColor::visit();
    ccDrawColor4F(1, 0, 0, 1);
    ccDrawLine(m_touchLStart, m_touchLPos);
}

void GameScene::shade(const CCPoint &p1, const CCPoint &p2)
{
    float l = 1000 * shadowScale;
    float d = 20 * shadowScale;
    ccColor4F c = ccc4f(0.05, 0.05, 0.05, 0.5);
    CCPoint p[4] = {p1 * shadowScale, p2 * shadowScale, CCPointZero, CCPointZero};
    CCPoint m = m_light->getPosition();
    
    p[3] = p[0] + (p[0] - m).normalize() * l;
    p[2] = p[1] + (p[1] - m).normalize() * l;
    ccDrawSolidPoly(p, 4, c);
    
    p[3] = p[0] + (p[0] - m + ccp(d,d)).normalize() * l;
    p[2] = p[1] + (p[1] - m + ccp(d,d)).normalize() * l;
    ccDrawSolidPoly(p, 4, c);
    
    p[3] = p[0] + (p[0] - m - ccp(d,d)).normalize() * l;
    p[2] = p[1] + (p[1] - m - ccp(d,d)).normalize() * l;
    ccDrawSolidPoly(p, 4, c);
    
    p[3] = p[0] + (p[0] - m + ccp(d,-d)).normalize() * l;
    p[2] = p[1] + (p[1] - m + ccp(d,-d)).normalize() * l;
    ccDrawSolidPoly(p, 4, c);
    
    p[3] = p[0] + (p[0] - m - ccp(d,-d)).normalize() * l;
    p[2] = p[1] + (p[1] - m - ccp(d,-d)).normalize() * l;
    ccDrawSolidPoly(p, 4, c);
}

bool GameScene::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
    m_dragging = true;
    m_dragPos = m_bg->convertToNodeSpace(pTouch->getLocation());
    m_dragStart = m_dragPos;
    if(pTouch->getLocation().x < m_screen.width * 0.5)
    {
        m_touchL = pTouch;
        m_touchLPos = m_touchLStart = pTouch->getLocation();
        
        m_pgAnim->setLoops(-1);
        m_pgAnimAction = CCAnimate::create(m_pgAnim);
        m_pg->runAction(m_pgAnimAction);

        //CCLOG("touchL");
    }
    else
    {
        // preserve direction
        CCPoint dir = (m_touchRPos - m_touchRStart).normalize() * 10;
        
        m_touchR = pTouch;
        m_touchRPos = pTouch->getLocation();
        m_touchRStart = m_touchRPos - dir;
        
        //CCLOG("touchR");
    }
    return true;
}

void GameScene::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
    m_dragPos = m_bg->convertToNodeSpace(pTouch->getLocation());
    if(pTouch == m_touchL)
    {
        m_touchLPos = pTouch->getLocation();
        //CCLOG("moveL");
    }
    if(pTouch == m_touchR)
    {
        m_touchRPos = pTouch->getLocation();
        //CCLOG("moveR");
    }
}

void GameScene::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
    m_dragging = false;
    //lines.push_back(Line(m_dragStart, m_dragPos));
    CCLOG("shade(ccp(%d, %d), ccp(%d, %d));",(int)m_dragStart.x, (int)m_dragStart.y,(int)m_dragPos.x, (int)m_dragPos.y);
    if(pTouch == m_touchL)
    {
        m_pgAnimAction->stop();
        m_pg->stopAllActions();
        m_touchL = NULL;
        //CCLOG("endL");
    }
    if(pTouch == m_touchR)
    {
        m_touchR = NULL;
        //CCLOG("endR");
    }
}

void GameScene::registerWithTouchDispatcher(void)
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, 0, true);
}
