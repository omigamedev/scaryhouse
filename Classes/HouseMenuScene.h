#ifndef __HOUSEMENUSCENE__
#define __HOUSEMENUSCENE__

#include "cocos2d.h"
using namespace cocos2d;

class HouseMenuScene : public CCLayer
{
    int m_menuIndex;
    CCMenu *m_menu;
    CCMenuItemImage *m_current;
    CCArray *m_houses;
    
    void onMenuNext(CCObject *target);
    void onMenuPrev(CCObject *target);
    void onMenuSelect(CCObject *target);
    void onMenuBack(CCObject *target);
    
    void navigate(int idx, float dir, bool anim = true);
    void navigateCallback(CCNode *target);
    
public:
    CREATE_FUNC(HouseMenuScene);
    static CCScene* scene()
    {
        CCScene* scene = CCScene::create();
        scene->addChild(create());
        return scene;
    }
    virtual bool init();
    virtual void onEnterTransitionDidFinish();
};

#endif
