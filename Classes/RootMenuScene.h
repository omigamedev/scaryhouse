#ifndef __ROOTMENUSCENE__
#define __ROOTMENUSCENE__

#include "cocos2d.h"

using namespace cocos2d;

class RootMenuScene : public CCLayer
{
    void onMenuStart(CCObject*);
    void onMenuExit(CCObject*);
public:
    CREATE_FUNC(RootMenuScene);
    static CCScene* scene()
    {
        CCScene *scene = CCScene::create();
        scene->addChild(RootMenuScene::create());
        return scene;
    }
    virtual bool init();
};

#endif
