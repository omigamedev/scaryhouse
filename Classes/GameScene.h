#ifndef __GAMESCENE__
#define __GAMESCENE_

#define CC_ENABLE_CHIPMUNK_INTEGRATION 1

#include "cocos2d.h"
#include "cocos-ext.h"
//#include "chipmunk.h"
#include <vector>

using namespace cocos2d;
using namespace extension;
using namespace std;

struct Wall
{
    CCPoint a, b;
    Wall(const CCPoint &start, const CCPoint &end)
    {
        a = start;
        b = end;
    }
};

struct Obstacle
{
    CCPoint tl, br;
    Obstacle(const CCPoint &topLeft, const CCPoint &bottomRight)
    {
        tl = topLeft;
        br = bottomRight;
    }
};

class GameScene : public CCLayerColor
{
    CCSprite *m_light;
    CCSprite *m_bg;
    CCPhysicsSprite *m_pg;
    CCSprite *m_pgShadow;
    CCAnimation *m_pgAnim;
    CCAnimate *m_pgAnimAction;
    CCSprite *m_light2;
    CCRenderTexture *m_shadow;
    float shadowScale;
    CCSize m_screen;
    bool m_dragging;
    CCPoint m_dragPos;
    CCPoint m_dragStart;
    vector<Wall> m_walls;
    vector<Obstacle> m_objs;
    CCTouch *m_touchL;
    CCTouch *m_touchR;
    CCPoint m_touchLStart;
    CCPoint m_touchRStart;
    CCPoint m_touchLPos;
    CCPoint m_touchRPos;
    
    cpSpace *m_space;

    void update(float dt);
    void shade(const CCPoint &p1, const CCPoint &p2);
    void addWall(const CCPoint &p1, const CCPoint &p2);
    void addObstacle(const CCPoint &p1, const CCPoint &p2);
    void renderShadows();
    
public:
    CREATE_FUNC(GameScene);
    static CCScene* scene()
    {
        CCScene *s = CCScene::create();
        s->addChild(create());
        return s;
    }
    virtual bool init();
    virtual void draw();
    virtual void visit();
    virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
    virtual void registerWithTouchDispatcher(void);
};

#endif
